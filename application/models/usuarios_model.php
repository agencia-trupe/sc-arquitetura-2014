<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'usuarios';

		if($this->input->post('password') != ''){

			$this->dados = array('username', 'senha', 'email');
			$this->dados_tratados = array(
				'senha' => sha1($this->input->post('password'))
			);
			
		}else{
			$this->dados = array('username', 'email');
			$this->dados_tratados = array();
		}
		
	}

/*
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
*/

}