<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oquefazemos_projetos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'o_que_fazemos_projetos';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('texto_etapa_1', 'texto_etapa_2', 'texto_etapa_3');
		$this->dados_tratados = array();
	}
}