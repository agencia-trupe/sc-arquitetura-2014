<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipe_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'quem_somos_equipe';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('imagem', 'titulo', 'texto', 'ordem');
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/equipe/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize_crop(239,336)
	         	  		->save($original['dir'].$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}
}