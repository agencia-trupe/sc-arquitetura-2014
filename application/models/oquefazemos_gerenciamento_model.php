<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oquefazemos_gerenciamento_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'o_que_fazemos_gerenciamento';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('texto');
		$this->dados_tratados = array();
	}
}