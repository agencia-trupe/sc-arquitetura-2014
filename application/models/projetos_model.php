<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'projetos';
		$this->tabela_imagens = 'projetos_imagens';
		$this->campo_rel = 'projetos_id';
		$this->campo_ordenacao = 'ordem';
		$this->tipo_ordenacao = 'ASC';

		$this->dados = array(
			'categoria_slug',
			'titulo',
			'texto',
			'olho',
			'capa',
			'ordem'
		);
		$this->dados_tratados = array(
			'capa' => $this->sobeCapa()
		);
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE && $v != 'ordem')
				$this->db->set($v, $this->input->post($v));
			elseif($v == 'ordem')
				$this->db->set($v, $this->inserePorUltimo());
		}
		$insert = $this->db->insert($this->tabela);

		$id = $this->db->insert_id();

		$this->db->set('slug', url_title($id.'-'.$this->input->post('titulo'), 'dash', TRUE))->where('id', $id)->update($this->tabela);

		return $insert;
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			$this->db->set('slug', url_title($id.'-'.$this->input->post('titulo'), 'dash', TRUE));

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function sobeCapa($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/projetos/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        	 	 ->load($original['dir'].$filename)
	         	  	 ->resize_crop(438,438)
	         		 ->save($original['dir'].'thumbs/grandes/'.$filename, TRUE)
	         		 ->resize_crop(260,260)
	         		 ->save($original['dir'].'thumbs/medias/'.$filename, TRUE)
	         		 ->resize_crop(173,177)
	         		 ->save($original['dir'].'thumbs/pequenas/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}		

	function pegarPorCategoria($categoria = ''){
		if($categoria)
			return $this->db->order_by($this->campo_ordenacao, $this->tipo_ordenacao)->get_where($this->tabela, array('categoria_slug' => $categoria))->result();
		else
			return $this->pegarTodos();
	}

	function imagens($tipo, $id_parent, $id_imagem = FALSE){

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';

		if(!$id_imagem){
			return $this->db->order_by('ordem', 'asc')->get_where($this->tabela_imagens, array($campo_rel_img => $id_parent, 'tipo' => $tipo))->result();
		}else{
			$query = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}	

	function inserirImagem($tipo){

		$imagem = $this->sobeImagem($tipo);

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';

		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem)
					 ->set('tipo', $tipo);
		
			return $this->db->set($campo_rel_img, $this->input->post($campo_rel_img))->insert($this->tabela_imagens);
		}else
			return false;
	}

	function editarImagem($tipo, $id_imagem){

		$imagem = $this->sobeImagem($tipo);

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';
		
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem)
					 ->set('tipo', $tipo);
		
			return  $this->db->set($campo_rel_img, $this->input->post($campo_rel_img))
							 ->where('id', $id_imagem)->update($this->tabela_imagens);
		}else
			return false;
	}

	function excluirImagem($tipo, $id_imagem){
		$imagem = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
		@unlink('_imgs/projetos/'.$tipo.'/'.$imagem[0]->imagem);
		@unlink('_imgs/projetos/'.$tipo.'/thumbs/'.$imagem[0]->imagem);
		return $this->db->delete($this->tabela_imagens, array('id' => $id_imagem));
	}		

	function sobeImagem($tipo){
		$this->load->library('upload');

		$original = array(
			'campo' => 'userfile',
			'dir' => '_imgs/projetos/'.$tipo.'/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        	 	 ->load($original['dir'].$filename)
	         	  	 ->resize(605, 605) // Será exibida como 625, 470
	         	  	 ->resize_crop(605, 470)
	         		 ->save($original['dir'].$filename, TRUE)
	         		 ->resize_crop(120,90)
	         		 ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}	
	}	
}