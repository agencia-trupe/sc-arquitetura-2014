<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'clippings';
		$this->tabela_imagens = 'clippings_imagens';

		$this->campo_ordenacao = 'data_publicacao';
		$this->tipo_ordenacao = 'DESC';
		$this->campo_rel = 'clippings_id';

		$this->dados = array(
			'titulo',
			'data_publicacao',
			'capa'
		);
		$this->dados_tratados = array(
			'data_publicacao' => formataData($this->input->post('data_publicacao'), 'br2mysql'),
			'capa' => $this->sobeCapa()
		);
	}

	function sobeCapa($campo = 'userfile'){
		$this->load->library('upload');
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/clippings/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize_crop(150,210)
	         	  		->save($original['dir'].'thumbs/'.$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

	function imagens($id_parent, $id_imagem = FALSE){

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';

		if(!$id_imagem){
			return $this->db->order_by('ordem', 'DESC')->get_where($this->tabela_imagens, array($campo_rel_img => $id_parent))->result();
		}else{
			$query = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function excluirImagem($id_imagem){
		$imagem = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
		@unlink('_imgs/clippings/'.$imagem[0]->imagem);
		return $this->db->delete($this->tabela_imagens, array('id' => $id_imagem));
	}		

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/clippings/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize(900, 900)
	         	  		->save($original['dir'].$filename, TRUE)
	         	  		->resize_crop(150,150)
	         	  		->save($original['dir'].'thumbs/'.$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}
}