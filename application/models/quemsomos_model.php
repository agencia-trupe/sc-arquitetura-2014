<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quemsomos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'quem_somos';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('texto');
		$this->dados_tratados = array();
	}
}