<?php
function formataData($data, $tipo){
    if($data != ''){
        if($tipo == 'br2mysql'){
            list($dia,$mes,$ano) = explode('/', $data);
            return $ano.'-'.$mes.'-'.$dia;
        }elseif($tipo == 'mysql2br'){
            list($ano,$mes,$dia) = explode('-', $data);
            return $dia.'/'.$mes.'/'.$ano;
        }
    }else{
        return false;
    }
}

function formataTimestamp($ts){
    list($data, $horas) = explode(' ', $ts);
    $data = formataHydros($data);
    $horas_arr = explode(':', $horas);
    unset($horas_arr[2]);
    return $data . ' | ' . implode(':', $horas_arr);
}

function dia($str){
    if(strpos($str, '/') !== FALSE){
        $xpd = explode('/', $str);
        return $xpd[0];
    }else{
        $xpd = explode('-', $str);
        return $xpd[2];
    }
}

function mes($str, $extenso = false){
    if($extenso){
        $arr = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro',
        );
    }else{
        $arr = array(
            '01' => 'jan',
            '02' => 'fev',
            '03' => 'mar',
            '04' => 'abr',
            '05' => 'mai',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'ago',
            '09' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez',
        );        
    }
    if(strpos($str, '/') !== FALSE){
        $xpd = explode('/', $str);
        return $arr[$xpd[1]];
    }elseif(strpos($str, '-') !== FALSE){
        $xpd = explode('-', $str);
        return $arr[$xpd[1]];
    }else{
        return $arr[$str];
    }
}

function ano($str){
    if(strpos($str, '/') !== FALSE){
        $xpd = explode('/', $str);
        return $xpd[2];
    }else{
        $xpd = explode('-', $str);
        return $xpd[0];
    }
}

function olho($text, $maxLength = 200){
    $wordArray = explode(' ', $text);
    if( sizeof($wordArray) > $maxLength ){
        $wordArray = array_slice($wordArray, 0, $maxLength);
        return implode(' ', $wordArray) . '&hellip;';
    }
    return $text;	
}

function relativizaUrl($str){
    $str = str_replace('../', '', $str);
    return $str;
}

function embed($url, $width = '670', $height = '408', $retorna_id = FALSE){

    if (strpos($url, 'youtube.com') !== FALSE) {
        $fonte = 'youtube';
    } elseif(strpos($url, 'youtu.be') !== FALSE) {
        $fonte = 'youtu.be';
    }elseif(strpos($url, 'vimeo.com') !== FALSE){
        $fonte = 'vimeo';
    }else{
        $fonte = 'id_video';
    }

    if($fonte == 'youtube'){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        $video_id = $my_array_of_vars['v'];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Youtube";
        }
    }elseif($fonte == 'vimeo'){
        preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe src='http://player.vimeo.com/video/".$video_id."' width='$width' height='$height' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Vimeo";
        }
    }elseif($fonte == 'youtu.be'){
        preg_match('@youtu.be/(.*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Youtube";
        }
    }elseif($fonte == 'id_video'){
        $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$url."' frameborder='0' allowfullscreen></iframe>";
        return $embed_str;
    }else{
        return "Erro ao incorporar o Vídeo";
    }
}

function videoThumb($url){
    $notfound = "_imgs/layout/no_video.jpg";

    if (strpos($url, 'youtube.com') !== FALSE) {
        $fonte = 'youtube';
    } elseif(strpos($url, 'youtu.be') !== FALSE) {
        $fonte = 'youtu.be';
    }elseif(strpos($url, 'vimeo.com') !== FALSE){
        $fonte = 'vimeo';
    }else{
        $fonte = 'id_video';
    }    

    if($fonte == 'youtube'){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        $video_id = $my_array_of_vars['v'];
        if($video_id){
            $thumb = "http://img.youtube.com/vi/".$video_id."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'vimeo'){
        preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            $xml = simplexml_load_file("http://vimeo.com/api/v2/video/".$video_id.".xml");
            foreach($xml->video as $node){
                $thumb = $node->thumbnail_small;
            }
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'youtu.be'){
        preg_match('@youtu.be/(.*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            $thumb = "http://img.youtube.com/vi/".$video_id."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'id_video'){
        if($url){
            $thumb = "http://img.youtube.com/vi/".$url."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }        
    }else{
        return $notfound;
    }
}

function limpaGMaps($str = '', $removeLink = TRUE){
    if($str != ''){
        if($removeLink)
            $str = preg_replace('~<br /><small>(.*)</small>~', '', $str);

        return mysql_real_escape_string(stripslashes($str));
    }else{
        return $str;
    }
}

function viewGMaps($str = '', $width = FALSE, $height = FALSE){

    $str = stripslashes(htmlspecialchars_decode($str));

    if($width)
        $str = preg_replace('~width="(\d+)"~', 'width="'.$width.'"', $str);
    if($height)
        $str = preg_replace('~height="(\d+)"~', 'height="'.$height.'"', $str);

    return $str;
}

function ip(){
    
    if(isset($_SERVER["REMOTE_ADDR"]))
        return $_SERVER["REMOTE_ADDR"]; 
    elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) 
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    elseif(isset($_SERVER["HTTP_CLIENT_IP"])) 
        return $_SERVER["HTTP_CLIENT_IP"];
        
}

function random_lipsum($amount = 1, $what = ‘paras’, $start = 0) {
    return simplexml_load_file('http://www.lipsum.com/feed/xml?amount='.$amount.'&what='.$what.'&start='.$start)->lipsum;
}
?>