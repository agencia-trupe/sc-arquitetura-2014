<div class="secao-banners">
	
	<div class="centro">

		<div class="pad-fundo">
			<div class="slides">

				<?php if ($banners): ?>
				
					<?php foreach ($banners as $key => $value): ?>
					
						<div class="slide">
							<img src="_imgs/banners/<?=$value->imagem?>" alt="<?=$value->titulo?>">
							<div class="texto"><?=$value->titulo?></div>
						</div>
						
					<?php endforeach ?>

				<?php endif ?>

			</div>
		</div>

	</div>

</div>