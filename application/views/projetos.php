<div class="secao-projetos">

	<div class="centro">
		
		<nav>
			<h1>PROJETOS</h1>
			<ul>
				<li>
					<a href="todos" class="ativo" title="Todos os Projetos">Todos</a>
				</li>
				<li>
					<a href="residencial" title="Projetos Residenciais">Residenciais</a>
				</li>
				<li>
					<a href="comercial" title="Projetos Comerciais">Comerciais</a>
				</li>
				<li>
					<a href="corporativo" title="Projetos Corporativos">Corporativos</a>
				</li>
			</ul>
		</nav>

	</div>

	<div class="galeria">

		<a href="#proj-nav-ant" class="nav nav-ant" title="Projetos Anteriores"><img src="_imgs/layout/prev.png" alt="Projetos Anteriores"></a>

		<div id="lista-projetos">

			<?php if ($projetos): ?>

				<?php
				$divGridAberto = false;
				$divOutrasAberta = false;
				$divMediasAberta = false;
				$divPequenasAberta = false;
				$contador = 0;
				?>
				
				<?php foreach ($projetos as $key => $value): ?>

					<?php if ($contador == 0): ?>
						
						<?php $divGridAberto = true; ?>
						<?php if ($key == 0): ?>
							<div class="grid">
						<?php else: ?>	
							<div class="grid" style="display:none;">
						<?php endif ?>

							<div class="grande">
								<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
									<img src="_imgs/projetos/thumbs/grandes/<?=$value->capa?>">
									<div class="overlay">
										<h2><?=$value->titulo?></h2>
										<?=$value->olho?>
									</div>
								</a>
							</div>

							<?php $contador++; ?>			

					<?php elseif($contador == 1): ?>
						
							<?php $divOutrasAberta = true; ?>
							<div class="outras">
								
								<?php $divMediasAberta = true; ?>
								<div class="medias">
									<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
										<img src="_imgs/projetos/thumbs/medias/<?=$value->capa?>">
										<div class="overlay">
											<h2><?=$value->titulo?></h2>
											<?=$value->olho?>
										</div>
									</a>

									<?php $contador++; ?>

					<?php elseif($contador == 2): ?>
								
									<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
										<img src="_imgs/projetos/thumbs/medias/<?=$value->capa?>">
										<div class="overlay">
											<h2><?=$value->titulo?></h2>
											<?=$value->olho?>
										</div>
									</a>
								</div>
								<?php $divMediasAberta = false; ?>

								<?php $contador++; ?>

					<?php elseif($contador == 3): ?>

								<?php $divPequenasAberta = true; ?>
								<div class="pequenas">
									<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
										<img src="_imgs/projetos/thumbs/pequenas/<?=$value->capa?>">
										<div class="overlay">
											<h2><?=$value->titulo?></h2>
											<?=$value->olho?>
										</div>
									</a>

									<?php $contador++; ?>

					<?php elseif($contador == 4): ?>
								
									<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
										<img src="_imgs/projetos/thumbs/pequenas/<?=$value->capa?>">
										<div class="overlay">
											<h2><?=$value->titulo?></h2>
											<?=$value->olho?>
										</div>
									</a>

									<?php $contador++; ?>
					
					<?php elseif($contador == 5): ?>
								
									<a href="pegarProjeto/<?=$value->slug?>" title="<?=$value->titulo?>">
										<img src="_imgs/projetos/thumbs/pequenas/<?=$value->capa?>">
										<div class="overlay">
											<h2><?=$value->titulo?></h2>
											<?=$value->olho?>
										</div>
									</a>

								<?php $divPequenasAberta = false; ?>
								</div>

							<?php $divOutrasAberta = false; ?>
							</div>

						<?php $divGridAberto = false; ?>
						</div>

						<?php $contador = 0; ?>

					<?php endif ?>

				<?php endforeach ?>

				<?php if ($divOutrasAberta): ?>
					</div>
				<?php endif ?>

				<?php if ($divMediasAberta): ?>
					</div>
				<?php endif ?>

				<?php if ($divPequenasAberta): ?>
					</div>
				<?php endif ?>

				<?php if ($divGridAberto): ?>
					</div>
				<?php endif ?>

			<?php endif ?>

		</div>

		<a href="#proj-nav-prox" class="nav nav-prox" title="Próximos Projetos"><img src="_imgs/layout/next.png" alt="Próximos Projetos"></a>

	</div>

</div>