<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Data de Publicação<br>
		<input type="text" name="data_publicacao" class="datepicker" value="<?=formataData($registro->data_publicacao, 'mysql2br')?>"></label>

		<label>Capa<br>
			<?if($registro->capa):?>
				<img src="_imgs/clippings/thumbs/<?=$registro->capa?>"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Data de Publicação<br>
		<input type="text" name="data_publicacao" class="datepicker"></label>

		<label>Capa<br>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>