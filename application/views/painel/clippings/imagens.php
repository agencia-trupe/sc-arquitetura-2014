<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
		<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
	    <h2>
	      <?if(!isset($registro[0])):?><a href="javascript: $('#form-ins').slideToggle('normal'); return false;" class="btn btn-success">Adicionar Imagem</a><?endif;?> <div style="max-width:700px;"><?=$titulo?></div>
	    </h2>    
  	</div>

  <a href="painel/<?=$this->router->class?>/index/" class="btn">← voltar</a>

  <br><br>

    <form method="post" id="form-ins" action="<?=base_url('painel/'.$this->router->class.'/inserirImagem')?>" enctype="multipart/form-data">

            <h3>Inserir Imagem</h3>

            <label>Imagem<br>                       
            <input type="file" name="userfile" required></label>

            <input type="hidden" name="clippings_id" value="<?=$parent->id?>">

            <div class="form-actions">
                <button class="btn btn-primary" type="submit">Salvar</button>                   
            </div>

    </form>

<br><br>

  <div class="row">
    <div class="span12 columns">

      <?php if ($imagens): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="clippings_imagens">

          <thead>
            <tr>
                <th>Ordenar</th>
                <th class="yellow header headerSortDown">Imagem</th>
                <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($imagens as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                        <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                        <td><img src="_imgs/clippings/thumbs/<?=$value->imagem?>" style="width:150px;"></td>
                        <td class="crud-actions" style="width:90px;">
                        	<a href="painel/<?=$this->router->class?>/excluirImagem/<?=$value->id.'/'.$parent->id?>" class="btn btn-danger btn-delete">excluir</a>
                        </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <h2>Nenhuma Imagem</h2>

      <?php endif ?>
    </div>
  </div>