<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

            <table class="table table-striped table-bordered table-condensed table-sortable">

                <thead>
                    <tr>
                        <th class="yellow header headerSortDown">Data de Publicação</th>
                        <th class="header">Título</th>
                        <th style="width:119px;">Imagens</th>
                        <th class="red header">Ações</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($registros as $key => $value): ?>

                    <tr class="tr-row" id="row_<?=$value->id?>">
                        <td><?= formataData($value->data_publicacao, 'mysql2br')?></td>
                        <td><?=$value->titulo?></td>
                        <td>
                            <a href="painel/<?=$this->router->class?>/imagens/<?=$value->id?>" class="btn" style="width:71px;">imagens</a>                            
                        </td>
                        <td class="crud-actions">
                            <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                            <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <?php if ($paginacao): ?>
            <div class="pagination">
                <ul>
                    <?=$paginacao?>
                </ul>
            </div>
            <?php endif ?>

            <?php else:?>

                <h3>Nenhum Registro</h2>

            <?php endif ?>

        </div>
    </div>