<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Endereço<br>
		<textarea name="endereco" class="medio basico"><?=$registro->endereco?></textarea></label><br>

		<label>Telefone<br>
		<textarea name="telefone" class="medio basico"><?=$registro->telefone?></textarea></label><br>
		
		<label>Facebook<br>			
		<input type="text" name="facebook" value="<?=$registro->facebook?>"></label><br>

		<label>Código de Incorporação do Google Maps
		<input type="text" name="mapa" value="<?= htmlentities($registro->mapa) ?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>