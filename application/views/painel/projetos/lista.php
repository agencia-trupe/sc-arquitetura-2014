<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
        </h2>
    </div>

    <div class="btn-group">
		<a href="painel/projetos/index/residencial" class="btn btn-small <?php if($filtro && $filtro == 'residencial'): ?> btn-warning <?php else: ?> btn-info <?php endif; ?>">Residenciais</a>
		<a href="painel/projetos/index/comercial" class="btn btn-small <?php if($filtro && $filtro == 'comercial'): ?> btn-warning <?php else: ?> btn-info <?php endif; ?>">Comerciais</a>
		<a href="painel/projetos/index/corporativo" class="btn btn-small <?php if($filtro && $filtro == 'corporativo'): ?> btn-warning <?php else: ?> btn-info <?php endif; ?>">Corporativos</a>
	</div><br><br>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

            <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="<?=$tabela_ordenacao?>">

                <thead>
                    <tr>
                        <?php if($filtro): ?>
                            <th>Ordenar</th>
                        <?php endif; ?>
                        <th class="header">Categoria</th>
                        <th class="yellow header headerSortDown">Título</th>
                        <th style="width:190px;">Imagens</th>
                        <th class="red header">Ações</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($registros as $key => $value): ?>

                    <tr class="tr-row" id="row_<?=$value->id?>">
                        <?php if($filtro): ?>
                            <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                        <?php endif; ?>
                        <td><?
                            switch($value->categoria_slug){
                                case 'residencial':
                                    echo "Residencial";
                                    break;
                                case 'comercial':
                                    echo "Comercial";
                                    break;
                                case 'corporativo':
                                    echo "Corporativo";
                                    break;
                            }
                        ?></td>
                        <td><?=$value->titulo?></td>
                        <td>
                            <a href="painel/<?=$this->router->class?>/imagens/antes/<?=$value->id?>" class="btn" style="width:71px;">antes</a>
                            <a href="painel/<?=$this->router->class?>/imagens/depois/<?=$value->id?>" class="btn" style="width:71px;">depois</a>
                        </td>
                        <td class="crud-actions">
                            <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                            <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <?php else:?>

                <h3>Nenhum Registro</h2>

            <?php endif ?>

        </div>
    </div>