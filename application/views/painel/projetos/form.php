<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Categoria<br>
			<select name="categoria_slug">
				<option value="residencial" <?php if($registro->categoria_slug == 'residencial') echo "selected" ?>>Residencial</option>
				<option value="comercial" <?php if($registro->categoria_slug == 'comercial') echo "selected" ?>>Comercial</option>
				<option value="corporativo" <?php if($registro->categoria_slug == 'corporativo') echo "selected" ?>>Corporativo</option>
			</select>
		</label>

		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno basico"><?=$registro->olho?></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="medio basico"><?=$registro->texto?></textarea></label>

		<label>Capa<br>
			<?if($registro->capa):?>
				<img src="_imgs/projetos/thumbs/grandes/<?=$registro->capa?>"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Categoria<br>
			<select name="categoria_slug">
				<option value="residencial">Residencial</option>
				<option value="comercial">Comercial</option>
				<option value="corporativo">Corporativo</option>
			</select>
		</label>

		<label>Título<br>
		<input type="text" name="titulo" required></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno basico"></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="medio basico"></textarea></label>

		<label>Capa<br>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>