<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">SC Arquitetura</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li <?if($this->router->class=='banners')echo" class='active'"?>><a href="painel/banners">Banners</a></li>

        <li <?if($this->router->class=='projetos')echo" class='active'"?>><a href="painel/projetos">Projetos</a></li>

        <li class="dropdown  <?if($this->router->class=='quemsomos' || $this->router->class=='equipe')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quem Somos <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/quemsomos/">Quem Somos</a></li>
            <li><a href="painel/equipe/">Equipe</a></li>
          </ul>
        </li>

        <li class="dropdown  <?if($this->router->class=='oquefazemos_projetos' || $this->router->class=='oquefazemos_gerenciamento')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">O Que Fazemos <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/oquefazemos_projetos">Projetos</a></li>
            <li><a href="painel/oquefazemos_gerenciamento">Gerenciamento</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='clippings')echo" class='active'"?>><a href="painel/clippings">Clippings</a></li>

        <li <?if($this->router->class=='contato')echo" class='active'"?>><a href="painel/contato">Contato</a></li>

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>