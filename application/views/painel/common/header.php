<!DOCTYPE html> 
<html lang="en-US" <?if(!$this->session->userdata('logged_in')) echo" class='login'"?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Painel de Administração - SC Arquitetura</title>
    
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2012 True Design" />	
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<base href="<?= base_url('painel') ?>">
	<script> var BASE = "<?= base_url('painel') ?>"</script>
	<script> var CLASSE = "<?= $this->router->class ?>"</script>

    <?CSS(array('painel/css/global', 'jquery-theme-lightness/jquery-ui-1.8.20.custom'))?>  
	<?JS(array('modernizr-2.0.6.min', 'jquery-1.8.0.min', 'tinymce/tiny_mce', 'jquery-ui-1.10.1.custom.min',
				'jquery.ui.datepicker-pt-BR', 'jquery.mtz.monthpicker', 'placeholder', 'painel/bootstrap', 'painel/bootbox.min', 'painel/admin'))?>

</head>
<body>