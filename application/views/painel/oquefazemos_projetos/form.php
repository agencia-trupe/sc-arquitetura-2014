<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto Etapa 1 - Preliminar<br>
		<textarea name="texto_etapa_1" class="medio basico"><?=$registro->texto_etapa_1?></textarea></label><br>

		<label>Texto Etapa 2 - Anteprojeto<br>
		<textarea name="texto_etapa_2" class="medio basico"><?=$registro->texto_etapa_2?></textarea></label><br>

		<label>Texto Etapa 3 - Projeto Executivo<br>
		<textarea name="texto_etapa_3" class="medio basico"><?=$registro->texto_etapa_3?></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto Etapa 1 - Preliminar<br>
		<textarea name="texto_etapa_1" class="medio basico"></textarea></label>

		<label>Texto Etapa 2 - Anteprojeto<br>
		<textarea name="texto_etapa_2" class="medio basico"></textarea></label>

		<label>Texto Etapa 3 - Projeto Executivo<br>
		<textarea name="texto_etapa_3" class="medio basico"></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>