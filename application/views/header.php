<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>SC Arquitetura - Projetos Arquitetônicos, Execução e Design</title>
  <meta name="description" content="SC ARQUITETURA Projeto, Execução e Design, projetamos sua casa e se preferir entregamos com a chave na mão. Profissionais qualificados.">
  <meta name="keywords" content="arquitetura, escritório de arquitetura, arquiteto, arquitetos, projeto arquitetônico, projeto de arquitetura" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2014 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content="SC ARQUITETURA"/>
  <meta property="og:site_name" content="SC ARQUITETURA"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="SC ARQUITETURA Projeto, Execução e Design, projetamos sua casa e se preferir entregamos com a chave na mão. Profissionais qualificados."/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'fontface', 'fancybox/jquery.fancybox', 'base'))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.11.0.min'))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>

<?if(ENVIRONMENT == 'development'):?>
  <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
<?endif;?>