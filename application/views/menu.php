<div class="secao-topo">
	<header>
		<div class="centro">

			<a href="<?=$contato->facebook?>" title="Nossa página no Facebook" target="_blank">
				<img src="_imgs/layout/face-icon.png" alt="Nossa página no Facebook">
			</a>

			<img src="_imgs/layout/logo-sc.png" alt="SC Arquitetura" id="logo">

		</div>
	</header>

	<nav>
		<a href="<?=base_url()?>" id="link-topo" title="Voltar ao Topo"><img src="_imgs/layout/logo-sc2.png" alt="SC Arquitetura"></a>
		<ul>
			<li><a href="projetos" title="Página Inicial">PROJETOS</a></li>
			<li><a href="quem-somos" title="Quem Somos">QUEM SOMOS</a></li>
			<li><a href="o-que-fazemos" title="O Que Fazemos">O QUE FAZEMOS</a></li>
			<li><a href="clippings" title="Clipping">CLIPPING</a></li>
			<li><a href="contato" title="Contato">CONTATO</a></li>
		</ul>		
	</nav>
</div>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">