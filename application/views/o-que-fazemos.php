<div class="secao-o-que-fazemos">
	
	<div class="centro">
		
		<nav>
			<h1>O QUE FAZEMOS</h1>
			<ul>
				<li>
					<a href="projetos" class="ativo" title="Projetos">Projetos</a>
				</li>
				<li>
					<a href="gerenciamento" title="Gerenciamento de Obra">Gerenciamento de Obra</a>
				</li>				
			</ul>
		</nav>

		<div class="texto aberto-p">

			<div style="overflow:hidden">

				<div class="textos-projetos secao">

					<h2>PROJETOS</h2>

					<div class="boxes preliminar-aberto">
					
						<div class="box preliminar">
							<?=$fazemos['projetos']->texto_etapa_1?>
						</div>

						<div class="box anteprojeto">
							<?=$fazemos['projetos']->texto_etapa_2?>
						</div>

						<div class="box projeto">
							<?=$fazemos['projetos']->texto_etapa_3?>
						</div>

					</div>

					<div class="botoes">
						<a href="preliminar" title="1. Preliminar" class="botao botao1 aberto">
							<div class="btexto">1.<br>PRELIMINAR</div>		
						</a>
						<a href="anteprojeto" title="2. Anteprojeto" class="botao botao2">
							<div class="btexto">2.<br>ANTEPROJETO</div>		
						</a>
						<a href="projeto" title="3. Projeto" class="botao botao3">
							<div class="btexto">3.<br>PROJETO</div>		
						</a>
					</div>

				</div>

				<div class="textos-gerenciamento secao">

					<h2>GERENCIAMENTO DE OBRA</h2>
					
					<div class="boxes">
						<div class="box gerenciamento">
							<?=$fazemos['gerenciamento']->texto?>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>