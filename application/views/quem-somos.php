<div class="secao-quem-somos">
	
	<div class="centro">
		
		<h1>Quem Somos</h1>

		<div class="texto">
			<?=$quemSomos->texto?>
		</div>

		<?php if ($equipe): ?>
			<span>
			<?php foreach ($equipe as $key => $value): ?>				
					
				<div class="equipe">
					<img src="_imgs/equipe/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<div class="overlay">
						<h2><?=$value->titulo?></h2>
						<?=$value->texto?>
					</div>
				</div>				

			<?php endforeach ?>
			</span>
		<?php endif ?>

	</div>

</div>