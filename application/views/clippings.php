<div class="secao-clippings">
	
	<div class="centro">

		<h1>CLIPPING</h1>

		<?php if ($clippings): ?>
			<div id="viewport">

				<a href="clip-nav-ant" class="nav nav-ant" title="Clippings Anteriores"><img src="_imgs/layout/prev.png" alt="Clippings Anteriores"></a>

				<div class="slides">

					<div class="slide">
					<?php $contador = 0 ?>
					<?php foreach ($clippings as $key => $value): ?>
						
						<a href="_imgs/clippings/<?=$value->capa?>" rel="view-<?=$value->id?>" title="<?=$value->titulo?>">
							<img src="_imgs/clippings/thumbs/<?=$value->capa?>" alt="<?=$value->titulo?>">
						</a>

						<?php if (isset($value->imagens) && $value->imagens): ?>
							<?php foreach ($value->imagens as $key => $img): ?>
								<a href="_imgs/clippings/<?=$img->imagem?>" rel="view-<?=$value->id?>" style="display:none;"></a>
							<?php endforeach ?>
						<?php endif ?>

						<?php $contador++ ?>

						<?php if ($contador%5==0 && $contador > 0): ?>
							</div><div class="slide" style="display:none;">
							<?php $contador=0 ?>
						<?php endif ?>
					
					<?php endforeach ?>
					</div>
				</div>

				<a href="clip-nav-prox" class="nav nav-prox" title="Próximos Clippings"><img src="_imgs/layout/next.png" alt="Próximos Clippings"></a>

			</div>
		<?php endif ?>

	</div>

</div>