<div class="detalhe">
	
	<div id="ampliada">
		<?php if (isset($detalhe->imagens['antes'][0])): ?>
			<img src="_imgs/projetos/antes/<?=$detalhe->imagens['antes'][0]->imagem?>">
		<?php elseif (isset($detalhe->imagens['depois'][0])): ?>
			<img src="_imgs/projetos/depois/<?=$detalhe->imagens['depois'][0]->imagem?>">
		<?php else: ?>
			<img src="_imgs/projetos/thumbs/grandes/<?=$detalhe->capa?>">
		<?php endif; ?>
	</div>

	<div class="descricao">
		<h2><?=$detalhe->titulo?></h2>
		<?=$detalhe->texto?>
		
		<div id="muda-tipo">
			<span>Compare o "antes e depois" deste projeto:</span>
			<a href="antes" title="Antes" class="ativo">Antes</a>
			<a href="depois" title="Depois">Depois</a>
		</div>
	</div>

	<div id="lista-thumbs">

		<?php if ($detalhe->imagens['antes']): ?>
			<ul class="thumbs antes">
				<?php foreach ($detalhe->imagens['antes'] as $key => $value): ?>					
					<li>
						<a href="_imgs/projetos/antes/<?=$value->imagem?>" <?php if($key == 0): ?> class="aberto" <?php endif; ?> title="Ver Imagem">
							<img src="_imgs/projetos/antes/thumbs/<?=$value->imagem?>" alt="Imagem - <?=$detalhe->titulo?> - Antes">
						</a>
					</li>
				<?php endforeach ?>
			</ul>
		<?php endif ?>

		<?php if ($detalhe->imagens['depois']): ?>
			<ul class="thumbs depois">
				<?php foreach ($detalhe->imagens['depois'] as $key => $value): ?>					
					<li>
						<a href="_imgs/projetos/depois/<?=$value->imagem?>" <?php if($key == 0): ?> class="aberto" <?php endif; ?> title="Ver Imagem">
							<img src="_imgs/projetos/depois/thumbs/<?=$value->imagem?>" alt="Imagem - <?=$detalhe->titulo?> - depois">
						</a>
					</li>
				<?php endforeach ?>
			</ul>
		<?php endif ?>

	</div>

	<a href="home" id="fechar-projeto" title="Voltar">Voltar</a>

</div>