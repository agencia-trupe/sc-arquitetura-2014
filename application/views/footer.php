
    </div> <!-- fim da div main -->

    <footer>

        <div class="centro">
      
            &copy; <?=date('Y')?> SC ARQUITETURA - Todos os direitos reservados.

            <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites: Trupe Agência Criativa <img src="_imgs/layout/trpe-icon.png" alt="Trupe Agência Criativa"></a>

        </div>
  
    </footer>
  
  
    <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
        <script>
            window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
            Modernizr.load({
                load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
            });
        </script>
    <?endif;?>

    <?JS(array('jquery.fancybox', 'fixedHeader', 'menuScroll', 'cycle', 'front'))?>
    
</body>
</html>
