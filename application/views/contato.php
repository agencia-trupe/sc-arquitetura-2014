<div class="secao-contato">
	
	<div class="centro">
		
		<h1>CONTATO</h1>

		<div id="form">
			
			<form action="home/enviarContato" method="post">
				
				<input type="text" name="nome" placeholder="Nome" required>

				<input type="email" name="email" placeholder="E-mail" required>

				<input type="text" name="assunto" placeholder="Assunto" required>

				<textarea name="mensagem" placeholder="Mensagem" required></textarea>

				<input type="submit" value="Enviar">

			</form>

			<div id="retorno" class="escondido"></div>

		</div>

		<div class="info">
			
			<h2>Endereço</h2>
			<?=$contato->endereco?>

			<h2>Ligue</h2>
			<?=$contato->telefone?>			

			<h2>Siga</h2>
			<a href="<?=prep_url($contato->facebook)?>" target="_blank" title="Nossa página no Facebook"><img src="_imgs/layout/face-icon.png" alt="Nossa página no Facebook"></a>

		</div>

		<div class="maps">
			<?= viewGMaps($contato->mapa, 960, 300) ?>
		</div>

	</div>

</div>