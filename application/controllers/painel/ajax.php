<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('painel/home');
   }

	function gravaOrdem(){
        $menu = $this->input->post('data');
        for ($i = 0; $i < count($menu); $i++) {
            $this->db->set('ordem', $i)
            		 ->where('id', $menu[$i])
            		 ->update($this->input->post('tabela'));
        }
	} 

}
