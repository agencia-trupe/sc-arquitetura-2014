<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipe extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Equipe";
		$this->unidade = "Membro";
		$this->load->model('equipe_model', 'model');
	}

}