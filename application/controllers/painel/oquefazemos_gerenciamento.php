<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oquefazemos_gerenciamento extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "O Que Fazemos - Gerenciamento";
		$this->unidade = "Texto";
		$this->load->model('oquefazemos_gerenciamento_model', 'model');
	}

}