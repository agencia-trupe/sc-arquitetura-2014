<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Contato";
		$this->unidade = "Informações de Contato";
		$this->load->model('contato_model', 'model');
	}

}