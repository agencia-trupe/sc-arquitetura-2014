<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('banners_model', 'model');   	
   }

   function index($pag = 0){

      $pag_options = array(
         'base_url' => base_url("painel/banners/index/"),
         'per_page' => 300,
         'uri_segment' => 4,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->model->numeroResultados()
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag);
      $data['titulo'] = "Banners";
      $data['unidade'] = "Banner";
      
      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

      $data['tabela_ordenacao'] = $this->model->tabela();

   	$this->load->view('painel/banners/lista', $data);
   }

   function form($id = false){

      if($id){
         $data['titulo'] = 'Editar Banner';
   	   $data['registro'] = $this->model->pegarPorId($id);
         if(!$data['registro'])
            redirect('painel/banners');
      }else{
         $data['titulo'] = 'Inserir Banner';
         $data['registro'] = FALSE;
      }
      
      $data['unidade'] = "Banner";
   	$this->load->view('painel/banners/form', $data);
   }

   function inserir(){
      if($this->model->inserir()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Banner inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Banner');
      }
      
      redirect('painel/banners/index/', 'refresh');
   }

   function alterar( $id){
      if($this->model->alterar($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Banner alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Banner');
      }     
   	redirect('painel/banners/index/', 'refresh');
   }

   function excluir($id){
	   if($this->model->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Banner excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Banner');
      }

      redirect('painel/banners/index/', 'refresh');
   }

}