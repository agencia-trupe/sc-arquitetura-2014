<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_controller {

	function __construct(){
		parent::__construct();
	}

    function index() {
        if($this->session->userdata('logged_in')){
            $data['usuario'] = $this->session->userdata('usuario');

            $this->load->view('painel/common/header');
            $this->load->view('painel/common/menu');
            $this->load->view('painel/home', $data);
            $this->load->view('painel/common/footer');
        }else{
            $data['mostrarerro'] = ($this->session->flashdata('errlogin') == true) ? "Usuário ou Senha incorretos" : false;
            $this->load->view('painel/common/header', $data);
            $this->load->view('painel/login');
        }
    }

    function login(){
        if(!$this->simplelogin->login($this->input->post('usuario'), $this->input->post('senha')))
            $this->session->set_flashdata('errlogin', true);

        redirect('painel');
    }

    function logout(){
        $this->simplelogin->logout();
        redirect('painel');
    }

}