<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oquefazemos_projetos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "O Que Fazemos - Projetos";
		$this->unidade = "Texto";
		$this->load->model('oquefazemos_projetos_model', 'model');
	}
	
    function index(){

        $data['registros'] = $this->model->pegarTodos();

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }
}