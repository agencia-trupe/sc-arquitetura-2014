<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Clipping";
		$this->unidade = "Clipping";
		$this->load->model('clippings_model', 'model');
	}

}