<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Projetos";
		$this->unidade = "Projeto";
		$this->load->model('projetos_model', 'model');
	}

    function index($filtro = false){        

    	if(!$filtro)
        	$data['registros'] = $this->model->pegarTodos();
        else
        	$data['registros'] = $this->model->pegarPorCategoria($filtro);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['filtro'] = $filtro;
        $data['tabela_ordenacao'] = $this->model->tabela();
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function imagens($tipo, $id_parent, $id_imagem =  FALSE){

        if($id_imagem){

            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($tipo, $id_parent);
            $data['registro'] = $this->model->imagens($id_parent, $id_imagem);
            
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);

        }else{
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($tipo, $id_parent);
            $data['registro'] = FALSE;
        }

        if(isset($data['parent']->titulo))
            $titulo_atual = $data['parent']->titulo;
        elseif(isset($data['parent']->nome))
            $titulo_atual = $data['parent']->nome;
        else
            $titulo_atual = $this->titulo;

        $data['campo_1'] = "Imagem";
        
        if($tipo == 'antes')
            $data['titulo'] = 'Galeria de imagens (ANTES) de : '.$titulo_atual;
        else
            $data['titulo'] = 'Galeria de imagens (DEPOIS) de : '.$titulo_atual;

        $data['unidade'] = $this->unidade;
        $data['tipo'] = $tipo;
        $this->load->view('painel/'.$this->router->class.'/imagens', $data);
    }

    function inserirImagem($tipo){
        if($this->model->inserirImagem($tipo)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$tipo.'/'.$this->input->post('projetos_id'), 'refresh');
    }

    function editarImagem($tipo, $id_imagem){
        if($this->model->editarImagem($tipo, $id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$tipo.'/'.$this->input->post('projetos_id'), 'refresh');
    }

    function excluirImagem($tipo, $id_imagem, $id_album){
        if($this->model->excluirImagem($tipo, $id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$tipo.'/'.$id_album, 'refresh');
    }    
}