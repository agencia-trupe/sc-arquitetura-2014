<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('banners_model', 'banners');
   		$this->load->model('projetos_model', 'projetos');
   		$this->load->model('quemsomos_model', 'quemsomos');
        $this->load->model('equipe_model', 'equipe');
        $this->load->model('oquefazemos_projetos_model', 'oquefazemos_projetos');
        $this->load->model('oquefazemos_gerenciamento_model', 'oquefazemos_gerenciamento');
        $this->load->model('clippings_model', 'clippings');
        $this->load->model('contato_model', 'contato');
   		
    }

    function index($pag = 0){

        $banners['banners'] = $this->banners->pegarTodos();

        $projetos['projetos'] = $this->projetos->pegarTodos();
        
        $quemSomos['quemSomos'] = $this->quemsomos->unico();
        $quemSomos['equipe'] = $this->equipe->pegarTodos();

        $fazemos['fazemos']['projetos'] = $this->oquefazemos_projetos->unico();
        $fazemos['fazemos']['gerenciamento'] = $this->oquefazemos_gerenciamento->unico();

        $clipping['clippings'] = $this->clippings->pegarTodos();

        foreach ($clipping['clippings'] as $key => $value)
            $value->imagens = $this->clippings->imagens($value->id);
        

    	$contato['contato'] = $this->contato->unico();

    	$this->load->view('header');
    	$this->load->view('menu', $contato);
   		$this->load->view('banners', $banners);
   		$this->load->view('projetos', $projetos);
   		$this->load->view('quem-somos', $quemSomos);
   		$this->load->view('o-que-fazemos', $fazemos);
   		$this->load->view('clippings', $clipping);
   		$this->load->view('contato', $contato);
   		$this->load->view('footer');
    }

    function pegarGaleria($categoria = ''){
        if($categoria && ($categoria == 'todos' || $categoria == 'residencial' || $categoria == 'comercial' || $categoria == 'corporativo')){

            if($categoria == 'todos')
                $projetos = $this->projetos->pegarTodos();
            else
                $projetos = $this->projetos->pegarPorCategoria($categoria);

            if($projetos){

                $divGridAberto = false;
                $divOutrasAberta = false;
                $divMediasAberta = false;
                $divPequenasAberta = false;
                $contador = 0;
                $retorno = '';                
                
                foreach ($projetos as $key => $value):

                    if ($contador == 0):
                        
                        $divGridAberto = true;
                        if ($key == 0):
                            $retorno .= '<div class="grid">';
                        else:  
                            $retorno .= '<div class="grid" style="display:none;">';
                        endif;

                            $retorno .= '<div class="grande">';
                                $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                    $retorno .= '<img src="_imgs/projetos/thumbs/grandes/'.$value->capa.'">';
                                    $retorno .= '<div class="overlay">';
                                        $retorno .= '<h2>'.$value->titulo.'</h2>';
                                        $retorno .= $value->olho;
                                    $retorno .= '</div>';
                                $retorno .= '</a>';
                            $retorno .= '</div>';

                            $contador++;           

                    elseif($contador == 1):
                        
                            $divOutrasAberta = true;
                            $retorno .= '<div class="outras">';
                                
                                $divMediasAberta = true;
                                $retorno .= '<div class="medias">';
                                    $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                        $retorno .= '<img src="_imgs/projetos/thumbs/medias/'.$value->capa.'">';
                                        $retorno .= '<div class="overlay">';
                                            $retorno .= '<h2>'.$value->titulo.'</h2>';
                                            $retorno .= $value->olho;
                                        $retorno .= '</div>';
                                    $retorno .= '</a>';

                                    $contador++;

                    elseif($contador == 2):
                                
                                    $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                        $retorno .= '<img src="_imgs/projetos/thumbs/medias/'.$value->capa.'">';
                                        $retorno .= '<div class="overlay">';
                                            $retorno .= '<h2>'.$value->titulo.'</h2>';
                                            $retorno .= $value->olho;
                                        $retorno .= '</div>';
                                    $retorno .= '</a>';
                                $retorno .= '</div>';
                                $divMediasAberta = false;

                                $contador++;

                    elseif($contador == 3):

                                $divPequenasAberta = true;
                                $retorno .= '<div class="pequenas">';
                                    $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                        $retorno .= '<img src="_imgs/projetos/thumbs/pequenas/'.$value->capa.'">';
                                        $retorno .= '<div class="overlay">';
                                            $retorno .= '<h2>'.$value->titulo.'</h2>';
                                            $retorno .= $value->olho;
                                        $retorno .= '</div>';
                                    $retorno .= '</a>';

                                    $contador++;

                    elseif($contador == 4):
                                
                                    $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                        $retorno .= '<img src="_imgs/projetos/thumbs/pequenas/'.$value->capa.'">';
                                        $retorno .= '<div class="overlay">';
                                            $retorno .= ' <h2>'.$value->titulo.'</h2>';
                                            $retorno .= $value->olho;
                                        $retorno .= '</div>';
                                    $retorno .= '</a>';

                                    $contador++;
                    
                    elseif($contador == 5):
                                
                                    $retorno .= '<a href="pegarProjeto/'.$value->slug.'" title="'.$value->titulo.'">';
                                        $retorno .= '<img src="_imgs/projetos/thumbs/pequenas/'.$value->capa.'">';
                                        $retorno .= '<div class="overlay">';
                                            $retorno .= '<h2>'.$value->titulo.'</h2>';
                                            $retorno .= $value->olho;
                                        $retorno .= '</div>';
                                    $retorno .= '</a>';

                                $divPequenasAberta = false;
                                $retorno .= '</div>';

                            $divOutrasAberta = false;
                            $retorno .= '</div>';

                        $divGridAberto = false;
                        $retorno .= '</div>';

                        $contador = 0;

                    endif;

                endforeach;

                if ($divOutrasAberta):
                    $retorno .= '</div>';
                endif;

                if ($divMediasAberta):
                    $retorno .= '</div>';
                endif;

                if ($divPequenasAberta):
                    $retorno .= '</div>';
                endif;

                if ($divGridAberto):
                    $retorno .= '</div>';
                endif;

                echo $retorno;
            }
        }
    }

    function pegarProjeto($slug = ''){
        if($slug){

            $data['detalhe'] = $this->projetos->pegarPorSlug($slug);

            $data['detalhe']->imagens['antes'] = $this->projetos->imagens('antes', $data['detalhe']->id);
            $data['detalhe']->imagens['depois'] = $this->projetos->imagens('depois', $data['detalhe']->id);

            if($data['detalhe']){
                echo $this->load->view('detalhes', $data, TRUE);
            }
        }
    }

    function enviarContato(){
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');

        if($nome && $email && $mensagem){
            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'mail';
            /*
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.puglieserevestimentos.com.br';
            $emailconf['smtp_user'] = 'noreply@puglieserevestimentos.com.br';
            $emailconf['smtp_pass'] = 'raZh6dS!2zu_';
            */
            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'noreply@scarquitetura.com.br';
            $fromname = 'SC Arquitetura';
            $to = 'contato@scarquitetura.com.br';
            $bcc = 'bruno@trupe.net';
            $assunto = 'Contato via Site';

            $email = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Assunto :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$assunto</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$mensagem</span>
</body>
</html>
EML;

            $plain = <<<EML
Nome :$nome\r\n
E-mail :$email\r\n
Assunto :$assunto\r\n
Mensagem :$mensagem
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            if($bcc)
                $this->email->bcc($bcc);
            $this->email->reply_to($email);

            $this->email->subject($assunto);
            $this->email->message($email);
            $this->email->set_alt_message($plain);

            if($this->email->send())
                echo "Mensagem enviada com sucesso!";
            else
                echo "Erro ao enviar a mensagem!";
        }else{
            echo "Erro ao enviar a mensagem!";
        }
    }

}