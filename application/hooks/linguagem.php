<?php

/*
 *  Hook pre-controller para setar a linguagem como pt caso
 *  não esteja definido nenhum valor
 */
function inicia_linguagem(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        $CI =& get_instance();
        
        if($CI->linguagem){
            $padrao = 'pt';
    
            if(!$CI->session->userdata('language'))
                $CI->session->set_userdata('language', $padrao);
    
            // Define o prefixo da tabela a ser usada dependendo da linguagem
            $CI->session->set_userdata('prefixo', $CI->session->userdata('language').'_');
    
            $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
        }
    }
}

/*
 *  Redirecionar de volta após requisição
 */
function grava_para_redirecionar(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        $CI =& get_instance();

        $CI->session->set_userdata('redirect', uri_string());
    }
}

?>