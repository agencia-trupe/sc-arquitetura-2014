<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model { 

	var $tabela, $tabela_imagens;
	var $dados, $dados_tratados;
	var $imagemOriginal, $imagemThumb;

	var $campo_ordenacao, $tipo_ordenacao, $campo_rel;

	function __construct(){
		parent::__construct();		
	}

	function tabela(){
		return $this->tabela;
	}

	function unico(){
		$q = $this->db->get($this->tabela)->result();
		if(isset($q[0]))
			return $q[0];
		else
			return false;
	}

	function pegarTodos($order_campo = 'id', $order = 'ASC'){
		
		if(isset($this->campo_ordenacao) && $this->campo_ordenacao != '')
			$order_campo = $this->campo_ordenacao;

		if(isset($this->tipo_ordenacao) && $this->tipo_ordenacao != '')
			$order = $this->tipo_ordenacao;

		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function numeroResultados(){
		return $this->db->get($this->tabela)->num_rows();
	}
	
	function pegarPorId($id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function pegarPorSlug($slug){
		$qry = $this->db->get_where($this->tabela, array('slug' => $slug))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){

		if(isset($this->campo_ordenacao) && $this->campo_ordenacao != '')
			$order_campo = $this->campo_ordenacao;

		if(isset($this->tipo_ordenacao) && $this->tipo_ordenacao != '')
			$order = $this->tipo_ordenacao;
		
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function inserir(){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE && $v != 'ordem')
				$this->db->set($v, $this->input->post($v));
			elseif($v == 'ordem')
				$this->db->set($v, $this->inserePorUltimo());
		}
		return $this->db->insert($this->tabela);
	}

	function inserePorUltimo(){
		$check = $this->db->select_max('ordem')->get($this->tabela)->result();
		if(isset($check[0]) && $check[0])
		 	return (int) $check[0]->ordem + 1;
		else
		 	return 1;
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

	function imagens($id_parent, $id_imagem = FALSE){

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';

		if(!$id_imagem){
			return $this->db->get_where($this->tabela_imagens, array($campo_rel_img => $id_parent))->result();
		}else{
			$query = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inserirImagem(){

		$imagem = $this->sobeImagem();

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';

		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem);
		
			return $this->db->set($campo_rel_img, $this->input->post($campo_rel_img))->insert($this->tabela_imagens);
		}else
			return false;
	}

	function editarImagem($id_imagem){

		$imagem = $this->sobeImagem();

		if($this->campo_rel != '')
			$campo_rel_img = $this->campo_rel;
		else
			$campo_rel_img = 'id_parent';
		
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem);
		
			return  $this->db->set($campo_rel_img, $this->input->post($campo_rel_img))
							 ->where('id', $id_imagem)->update($this->tabela_imagens);
		}else
			return false;
	}

	function excluirImagem($id_imagem){
		$imagem = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
		@unlink('../'.$this->imagemOriginal['dir'].$imagem[0]->imagem);
		@unlink('../'.$this->imagemThumb['dir'].$imagem[0]->imagem);
		return $this->db->delete($this->tabela_imagens, array('id' => $id_imagem));
	}		

	/* $original = array(
		'dir' => '',
		'x' => '',
		'y' => '',
		'corte' => 'resize|resize_crop',
		'campo' => ''
	)*/
	/* $thumb = array('dir' => '', 'x' => '', 'y' => '', 'corte' => 'resize|resize_crop')*/
	function sobeImagem(){
		$this->load->library('upload');

		$original = $this->imagemOriginal;
		$thumb = $this->imagemThumb;
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE);
		        }

		        if(isset($thumb['x']) && $thumb['x'] && isset($thumb['y']) && $thumb['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$thumb['corte']($thumb['x'], $thumb['y'])
		                ->save($thumb['dir'].$filename, TRUE);	
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}