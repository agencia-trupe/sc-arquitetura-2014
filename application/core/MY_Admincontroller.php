<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Admincontroller extends CI_controller {

    var $headervar;
    var $menuvar;
    var $footervar;
    var $titulo,$unidade,$campo_1,$campo_2,$campo_3;
    var $hasLayout;

    function __construct() {
        parent::__construct();

        $this->hasLayout = TRUE;
        
        if(!$this->session->userdata('logged_in'))
            redirect('painel/home/index');

        if($this->session->flashdata('mostrarerro') === true){
            $this->headervar['mostrarerro'] = true;
            $this->menuvar['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $this->headervar['mostrarerro'] = false;
            $this->menuvar['mostrarerro_mensagem'] = '';
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $this->headervar['mostrarsucesso'] = true;
            $this->menuvar['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $this->headervar['mostrarsucesso'] = false;
            $this->menuvar['mostrarsucesso_mensagem'] = '';
        }
    }

    function index($pag = 0){
        $this->load->library('pagination');

          $pag_options = array(
             'base_url' => base_url("painel/".$this->router->class."/index/"),
             'per_page' => 20,
             'uri_segment' => 4,
             'next_link' => "Próxima →",
             'next_tag_open' => "<li class='next'>",
             'next_tag_close' => '</li>',
             'prev_link' => "← Anterior",
             'prev_tag_open' => "<li class='prev'>",
             'prev_tag_close' => '</li>',
             'display_pages' => TRUE,
             'num_links' => 10,
             'first_link' => FALSE,
             'last_link' => FALSE,
             'num_tag_open' => '<li>',
             'num_tag_close' => '</li>',
             'cur_tag_open' => '<li><b>',
             'cur_tag_close' => '</b></li>',
             'total_rows' => $this->model->numeroResultados()
          );

          $this->pagination->initialize($pag_options);
          $data['paginacao'] = $this->pagination->create_links();

          $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag);

          if($this->session->flashdata('mostrarerro') === true)
             $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
          else
             $data['mostrarerro'] = false;
         
          if($this->session->flashdata('mostrarsucesso') === true)
             $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
          else
             $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function inserir(){
        if($this->model->inserir()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function excluir($id){
        if($this->model->excluir($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function imagens($id_parent, $id_imagem =  FALSE){
        if($id_imagem){
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($id_parent);
            $data['registro'] = $this->model->imagens($id_parent, $id_imagem);
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($id_parent);
            $data['registro'] = FALSE;
        }

        if(isset($data['parent']->titulo))
            $titulo_atual = $data['parent']->titulo;
        elseif(isset($data['parent']->nome))
            $titulo_atual = $data['parent']->nome;
        else
            $titulo_atual = $this->titulo;

        $data['campo_1'] = "Imagem";
        $data['titulo'] = 'Galeria de imagens de : '.$titulo_atual;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/imagens', $data);
    }

    function inserirImagem(){
        if($this->model->inserirImagem()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('clippings_id'), 'refresh');
    }

    function editarImagem($id_imagem){
        if($this->model->editarImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('clippings_id'), 'refresh');
    }

    function excluirImagem($id_imagem, $id_album){
        if($this->model->excluirImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$id_album, 'refresh');
    }


    function _output($output){
        if($this->hasLayout)
          echo $this->load->view('painel/common/header', $this->headervar, TRUE).
               $this->load->view('painel/common/menu', $this->menuvar, TRUE).
               $output.
               $this->load->view('painel/common/footer', $this->footervar, TRUE);
        else
          echo $output;
    }
}
?>
