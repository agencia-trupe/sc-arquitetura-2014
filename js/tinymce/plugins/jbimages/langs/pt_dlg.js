tinyMCE.addI18n('pt.jbimages_dlg',{
	title : 'Fazer Upload de uma imagem do computador',
	select_an_image : 'Selecione uma Imagem',
	upload_in_progress : 'Subindo o arquivo',
	upload_complete : 'Upload completo',
	upload : 'Upload',
	longer_than_usual : 'A operação está demorando mais que o usual',
	maybe_an_error : 'Um erro pode ter ocorrido',
	view_output : 'Ver o resultado',
	
	lang_id : 'portuguese' /* php-side language files are in: ci/application/language/{lang_id}; and in ci/system/language/{lang_id} */
});