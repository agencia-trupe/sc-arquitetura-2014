$('document').ready( function(){

    // Botão para o Topo
	$('.secao-topo nav #link-topo').click( function(e){
		e.preventDefault();

		$('.secao-topo nav ul li a.ativo').removeClass('ativo');

		window.location.hash = 'home';

		$('html, body').animate({
			scrollTop : 0
		}, 600);
	});

    // Botões do Menu
	$('.secao-topo nav ul li a').click( function(e){
		e.preventDefault();

		var alvo = $(this).attr('href');

        if(!$(this).hasClass('ativo')){
            $('.secao-topo nav ul li a.ativo').removeClass('ativo');
            $(this).addClass('ativo');
        }
		
		window.location.hash = alvo;

        if(alvo == 'projetos')
            h = $('.secao-'+alvo).offset().top - 105;
        else if(alvo == 'clippings')
            h = $('.secao-'+alvo).offset().top - 90;
        else if($('.secao-'+alvo).length)
            h = $('.secao-'+alvo).offset().top - 74;
        else
            h = 0;

		$('html, body').animate({
			scrollTop : h
		}, 600);
	});

    // Abre o hash caso exista ao carregar a página
    if(window.location.hash){
        var alvo = window.location.hash;

        if(alvo.substring(0,1) == '#')
            alvo = alvo.substring(1);

        if(alvo == 'projetos')
            h = $('.secao-'+alvo).offset().top - 105;
        else if(alvo == 'clippings')
            h = $('.secao-'+alvo).offset().top - 90;
        else if($('.secao-'+alvo).length)
            h = $('.secao-'+alvo).offset().top - 74;
        else
            h = 0;
        
        $('html, body').animate({
            scrollTop : h
        }, 600);
    }

    
    window.addEventListener( 'scroll', function( event ) {
        var scrollY = window.pageYOffset || document.documentElement;

        var alturaProjetos = $('.secao-projetos').offset().top - 105,
        	alturaQuemsomos = $('.secao-quem-somos').offset().top - 74,
        	alturaFazemos = $('.secao-o-que-fazemos').offset().top - 74,
        	alturaClipping = $('.secao-clippings').offset().top - 140,
        	alturaContato = $('.secao-contato').offset().top - 90;

        if(scrollY >= alturaProjetos && scrollY < alturaQuemsomos){
        	var ind = 0; // projetos
        }else if(scrollY >= alturaQuemsomos && scrollY < alturaFazemos){
        	var ind = 1; // quem somos
        }else if(scrollY >= alturaFazemos && scrollY < alturaClipping){
        	var ind = 2; // o que fazemos
        }else if(scrollY >= alturaClipping && scrollY < alturaContato){
        	var ind = 3; // clipping
        }else if(scrollY >= alturaContato){
        	var ind = 4; // contato
        }

        var marcar = $('.secao-topo nav ul li').eq(ind).find('a');
        if(marcar.length && !marcar.hasClass('ativo')){
        	$('.secao-topo nav ul li a.ativo').removeClass('ativo');
        	marcar.addClass('ativo');
            window.location.hash = marcar.attr('href');
        }
    }, false );
    
});