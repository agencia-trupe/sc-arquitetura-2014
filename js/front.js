var css_loader = "<div id='circleG'><div id='circleG_1' class='circleG'></div><div id='circleG_2' class='circleG'></div><div id='circleG_3' class='circleG'></div></div>";

var cycleProjetos = function(){
	if($('.secao-projetos .galeria #lista-projetos .grid').length > 1){

		$('.secao-projetos .galeria .nav-ant').css({ 'opacity' : 1});
		$('.secao-projetos .galeria .nav-prox').css({ 'opacity' : 1});

		$('.secao-projetos .galeria #lista-projetos').cycle({
			slides : '.grid',
			timeout : 0,
			prev : $('.secao-projetos .galeria .nav-ant'),
			next : $('.secao-projetos .galeria .nav-prox'),
			fx : 'scrollHorz'
		});
	}else{
		$('.secao-projetos .galeria .nav-ant').css({ 'opacity' : 0});
		$('.secao-projetos .galeria .nav-prox').css({ 'opacity' : 0});
	}
};

var bindProjetos = function(){
	$('.secao-projetos .galeria #lista-projetos .grid a').click( function(e){
		e.preventDefault();

		var tgt = $(this).attr('href');

		if(!$('.secao-projetos .galeria').hasClass('detalhando'))
			$('.secao-projetos .galeria').addClass('detalhando');

		setTimeout( function(){
			$('.galeria').append("<div class='loading'>"+css_loader+"</div>");
			$('.galeria .loading').addClass('ativo');

			$.ajax({
				url : 'home/' + tgt
			}).done( function(retorno){
				$('.secao-projetos .galeria #lista-projetos').cycle('destroy');
				$('.galeria #lista-projetos').remove();
				$('.galeria').append("<div id='lista-projetos'>"+retorno+"</div>");
				$('.galeria #lista-projetos').css({opacity:0});
				
				if(!$('.secao-projetos .galeria #lista-projetos').hasClass('rel'))
					$('.secao-projetos .galeria #lista-projetos').addClass('rel');

				bindFecharProjeto();
				bindTrocarThumbs();
				bindAbrirFoto();

				setTimeout( function(){
					cycleProjetos();
					$('.galeria .loading').removeClass('ativo');
					$('.galeria #lista-projetos').css({opacity:1});
					setTimeout( function(){
						$('.galeria .loading').remove();
					}, 500);
				}, 1000);
			});
		},300);
	});
};

var bindFecharProjeto = function(){
	$('#fechar-projeto').click( function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop : $('.secao-projetos').offset().top - 105
		}, 600);
		$('.secao-projetos nav ul li a.ativo').removeClass('ativo').click();
	});
};

var bindTrocarThumbs = function(){

	if($('#lista-thumbs .thumbs.depois').length || $('#lista-thumbs .thumbs.antes').length){

		$('#lista-thumbs .thumbs.depois').fadeOut();

		$('#muda-tipo a').click( function(e){
			e.preventDefault();
			
			var alvo = $(this).attr('href');
			
			if(!$(this).hasClass('ativo')){

				$('#muda-tipo a.ativo').removeClass('ativo');
				$(this).addClass('ativo');

				if(alvo == 'antes'){
					$('#lista-thumbs .thumbs.depois').fadeOut( function(){
						$('#lista-thumbs .thumbs.antes').fadeIn();				
					});
				}else if(alvo == 'depois'){
					$('#lista-thumbs .thumbs.antes').fadeOut( function(){
						$('#lista-thumbs .thumbs.depois').fadeIn();				
					});
				}
			}
		});

	}else{
		$('#muda-tipo').hide();
	}
};

var bindAbrirFoto = function(){
	$('#lista-thumbs .thumbs li a').click( function(e){
		e.preventDefault();
		var alvo = $(this).attr('href');
		if(!$(this).hasClass('aberto')){
			$('#lista-thumbs .thumbs li a.aberto').removeClass('aberto');
			$(this).addClass('aberto');
			$('#ampliada img').attr('src', alvo);
		}
	});
};


$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	fixedHeader;

	$('.secao-banners .slides').cycle({
		slides : '.slide'
	});

	$('.secao-projetos nav ul li a').click( function(e){
		e.preventDefault();

		if(!$(this).hasClass('ativo')){

			var categoria = $(this).attr('href');			

			$('.secao-projetos nav ul li a.ativo').removeClass('ativo');
			$(this).addClass('ativo');

			if($('.secao-projetos .galeria').hasClass('detalhando'))
				$('.secao-projetos .galeria').removeClass('detalhando');

			$('.galeria').append("<div class='loading'>"+css_loader+"</div>");
			$('.galeria .loading').addClass('ativo');

			$.ajax({
				url : "home/pegarGaleria/"+categoria
			}).done(function(retorno){
				$('.secao-projetos .galeria #lista-projetos').cycle('destroy');
				$('.galeria #lista-projetos').remove();
				$('.galeria').append("<div id='lista-projetos'>"+retorno+"</div>");
				setTimeout( function(){
					cycleProjetos();
					bindProjetos();
					$('.galeria .loading').removeClass('ativo');
					setTimeout( function(){
						$('.galeria .loading').remove();
					}, 500);
				}, 1000);
			});
		}
	});

	$('.secao-o-que-fazemos nav ul li a').click( function(e){
		e.preventDefault();
		if(!$(this).hasClass('ativo')){
			
			$('.secao-o-que-fazemos nav ul li a.ativo').removeClass('ativo');
			$(this).addClass('ativo');

			if($(this).attr('href') == 'projetos'){
				$('.secao-o-que-fazemos .texto').removeClass('aberto-g').addClass('aberto-p');
			}else if($(this).attr('href') == 'gerenciamento'){
				$('.secao-o-que-fazemos .texto').removeClass('aberto-p').addClass('aberto-g');
			}
		}
	});

	$('.secao-o-que-fazemos .texto .botoes a').click( function(e){
		e.preventDefault();

		if(!$(this).hasClass('aberto')){

			$('.secao-o-que-fazemos .texto .botoes a.aberto').removeClass('aberto');
			$(this).addClass('aberto');

			var alvo = $(this).attr('href');

			$('.textos-projetos .boxes').removeClass('preliminar-aberto');
			$('.textos-projetos .boxes').removeClass('anteprojeto-aberto');
			$('.textos-projetos .boxes').removeClass('projeto-aberto');

			if(alvo == 'preliminar'){
				$('.textos-projetos .boxes').addClass('preliminar-aberto');
			}else if(alvo == 'anteprojeto'){
				$('.textos-projetos .boxes').addClass('anteprojeto-aberto');
			}else if(alvo == 'projeto'){
				$('.textos-projetos .boxes').addClass('projeto-aberto');
			}
		}
	});

	if($('.secao-clippings #viewport .slides .slide').length > 1){
		$('.secao-clippings #viewport .slides').cycle({
			slides : '.slide',
			timeout : 0,
			prev : $('.secao-clippings #viewport .nav.nav-ant'),
			next : $('.secao-clippings #viewport .nav.nav-prox'),
			fx : 'scrollHorz'
		});
	}else{

	}

	$('.secao-clippings .slides .slide a').fancybox({
		padding : 0,
		helpers : {
			title : null,
			overlay : {
				css : {
					'background-color' : "url('rgba.php/rgba(70,41,85,.8)')",
					'background-color' : "rgba(70,41,85,.8)"
				}
			}
		}
	});

	$('form').submit( function(e){
		e.preventDefault();

		var nome = $("input[name=nome]").val(),
			email = $("input[name=email]").val(),
			assunto = $("input[name=assunto]").val(),
			mensagem = $("textarea[name=mensagem]").val(),
			destino = $(this).attr('action');

		if(nome == ''){
			alert('Informe seu nome!');
			return false;
		}
		if(email == ''){
			alert('Informe seu e-mail!');
			return false;
		}
		if(assunto == ''){
			alert('Informe o assunto da mensagem!');
			return false;
		}
		if(mensagem == ''){
			alert('Informe sua mensagem!');
			return false;
		}

		$.post(destino, {
			nome : nome,
			email : email,
			assunto : assunto,
			mensagem : mensagem
		}, function(retorno){
			$('#form form').addClass('escondido');
			$('#retorno').html(retorno).removeClass('escondido');
			setTimeout( function(){
				$('form input[type=text],form input[type=email],form textarea').val('');
				$('#form form').removeClass('escondido');
				$('#retorno').addClass('escondido');
			}, 3000);
		});
	});

	cycleProjetos();
	bindProjetos();


});