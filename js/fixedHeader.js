var fixedHeader = (function() {
 
    var docElem = document.documentElement,
        header = $('.secao-topo'),
        didScroll = false,
        changeHeaderOn = 295;
 
    function init() {
        window.addEventListener( 'scroll', function( event ) {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( scrollPage, 250 );
            }
        }, false );
    }
 
    function scrollPage() {
        var sy = scrollY();
        if ( sy >= changeHeaderOn ) {
            header.addClass('fixo');
            //$('body').css('padding-top', 435);
        }
        else {
            header.removeClass('fixo');
            //$('body').css('padding-top', 0);
        }
        didScroll = false;
    }
 
    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }
 
    init();
 
})();