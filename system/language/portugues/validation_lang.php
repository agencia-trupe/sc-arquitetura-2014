<?php

$lang['required'] = 'O campo %s � obrigat�rio.';
$lang['isset'] = 'O campo %s deve ter algum valor.';
$lang['valid_email'] = 'O campo %s deve conter um endere�o de e-mail v�lido.';
$lang['valid_emails'] = 'O campo %s deve conter todos os endere�os de e-mail v�lidos.';
$lang['valid_url'] = 'O campo %s deve conter uma URL v�lida';
$lang['valid_ip'] = 'O campo %s deve conter um endere�o IP v�lido.';
$lang['min_length'] = 'O campo %s deve ter pelo menos %s caracteres em comprimento.';
$lang['max_length'] = 'O campo %s n�o pode exceder a quantidade de %s caracteres.';
$lang['exact_length'] = 'O campo %s deve ter exatamente %s caracteres em comprimento.';
$lang['alpha'] = 'O campo %s deve conter somente caraceteres do alfabeto.';
$lang['alpha_numeric'] = 'O campo %s deve conter somente caracteres alfa-num�ricos.';
$lang['alpha_dash'] = 'O campo %s deve conter somente caracteres alfa-num�ricos, sublinhados, e h�fens.';
$lang['numeric'] = 'O campo %s deve conter um n�mero.';
$lang['integer'] = 'O campo %s deve conter um inteiro.';
$lang['matches'] = 'O campo %s n�o corresponde a um campo %s.';
?>