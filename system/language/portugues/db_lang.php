<?php

$lang['db_invalid_connection_str'] = 'N�o � poss�vel determinar as configura��es do banco de dados com base nas informa��es de conex�o que voc� enviou.';
$lang['db_unable_to_connect'] = 'N�o � poss�vel conectar ao servidor de banco de dados usando as configura��es fornecidas.';
$lang['db_unable_to_select'] = 'N�o � poss�vel selecionar o banco de dados especificado: %s';
$lang['db_unable_to_create'] = 'N�o � poss�vel criar o banco de dados especificado: %s';
$lang['db_invalid_query'] = 'A consulta que voc� submeteu n�o � v�lida.';
$lang['db_must_set_table'] = 'Voc� deve definir a tabela de banco de dados para ser usada com a sua consulta.';
$lang['db_must_use_set'] = 'Voc� deve usar o m�todo  "SET" para atualizar uma entrada.';
$lang['db_must_use_index'] = 'Voc� deve especificar um �ndice para coincidir com as atualiza��es em lote.';
$lang['db_batch_missing_index'] = 'Uma ou mais linhas apresentadas do lote est� faltando para atualizar o �ndice especificado.';
$lang['db_must_use_where'] = 'Atualiza��es n�o s�o permitidos, a menos que eles cont�m um "WHERE" cl�usula.';
$lang['db_del_must_use_where'] = 'N�o � permitido excluir a menos que ele contenha "WHERE" ou "LIKE" na cl�usa.';
$lang['db_field_param_missing'] = 'Para retornar os campos, � necess�rio o nome da tabela como um parâmetro.';
$lang['db_unsupported_function'] = 'Este recurso n�o est� dispon�vel para o banco de dados voc� est� usando.';
$lang['db_transaction_failure'] = 'Falha na transa��o: um Rollback foi executado.';
$lang['db_unable_to_drop'] = 'Incapaz executar "DROP" no banco de dados especificado.';
$lang['db_unsuported_feature'] = 'Recurso n�o suportado na plataforma de banco de dados voc� est� usando.';
$lang['db_unsuported_compression'] = 'O formato de compress�o de arquivo que voc� escolheu n�o � suportado pelo seu servidor.';
$lang['db_filepath_error'] = 'N�o � poss�vel escrever no caminho do arquivo que voc� enviou.';
$lang['db_invalid_cache_path'] = 'O caminho do cache que voc� apresentou n�o � v�lido ou n�o possui permiss�o de escrita.';
$lang['db_table_name_required'] = 'Um nome de tabela � necess�rio para essa opera��o.';
$lang['db_column_name_required'] = 'O nome da coluna � necess�rio para esta opera��o.';
$lang['db_column_definition_required'] = '� necess�rio definir uma coluna para esta opera��o.';
$lang['db_unable_to_set_charset'] = 'N�o � poss�vel conectar-se com o cliente com o charset definido: %s';
$lang['db_error_heading'] = 'Ocorreu um erro no banco de dados';
?>