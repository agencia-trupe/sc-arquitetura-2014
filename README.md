# NOME DO SITE #

* Site Gerenciável

* CodeIgniter 2.1

TODO: Separar 2 repositórios, um de multi-línguas e o outro só de português

# Controllers do Painel #

ajax.php : Para requisições via ajax.
	- gravaOrdem() : Grava a ordem dos itens ordenáveis nas tabelas

banners.php : Cadastro de Banners da home

blog.php : Sistema de blogs (posts, categorias e comentários)

cadastros.php : Cadastro de Newsletters

contato.php : Cadastro de informações de contato - usar como modelo para conteúdo simples

home.php : Controller Inicial

noticias.php : Cadastro de Notícias padrão

usuarios.php : Cadastro de Usuários do Painel